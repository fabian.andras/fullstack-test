import React from 'react';
import { Button, Table, TableContainer, TableHead, TableBody, TableRow, TableCell, CircularProgress } from '@material-ui/core'
import MovieRow from './MovieRow'
import request from './request';

class MovieTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      movies: props.movies,
      showSimilarTo: null
    }
  }

  getSimilarMovies = async (idx) => {
    this.setState({
      loading: true
    });
    let id = this.state.movies[idx].id
    let query = [
      'query getMovie {',
      '  movie(id: "' + id + '") {',
      '    id',
      '    similar(limit: 20) {',
      '      id',
      '      name',
      '      score',
      '      genres {',
      '        name',
      '      }',
      '    }',
      '  }',
      '}'
    ].join('\n');
    
    let payload = {
      operationName: 'getMovie',
      query: query,
      variables: {}
    }
    let result = await request('POST', 'https://tmdb.sandbox.zoosh.ie/dev/graphql', payload);
    this.setState({
      loading: false
    })
    return result.data.movie.similar
  }

  showSimilar = async (idx) => {
    if (idx != null) {
      let result = await this.getSimilarMovies(idx)
      this.setState({
        movies: result,
        showSimilarTo: idx
      })
    } else {
      this.setState({
        movies: this.props.movies,
        showSimilarTo: null
      })
    }
  }

  getHeaderText = () => {
    if (this.state.showSimilarTo !== null) {
      return (
        <TableRow>
          <TableCell colSpan="3">
            <h1 className="LeftPad">
              Similar to: {this.props.movies[this.state.showSimilarTo].name}
              <Button color="secondary" variant="outlined" onClick={() => this.showSimilar(null)}>Back to movie</Button>
            </h1>
          </TableCell>
        </TableRow>
      )
    }
    return null
  }

  getTable() {
    if (this.state.loading) {
      return (
        <CircularProgress id="CircularProgress" />
      )
    }
    return (
      <TableContainer>
        <Table>
          <TableHead>
            {this.getHeaderText()}
            <TableRow>
              <TableCell>Title</TableCell>
              <TableCell>Categories</TableCell>
              <TableCell>Score</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              this.state.movies.map((movie, i) => {
                return (
                  <MovieRow key={'movie-' + (this.state.showSimilarTo === null ? 'movie-' : 'similar-') + i} idx={i} url={movie.id} title={movie.name} score={movie.score} genres={movie.genres.map(genre => genre.name).join(', ')} showSimilar={this.showSimilar} showingSimilar={this.state.showSimilarTo !== null} />
                )
              })
            }

          </TableBody>
        </Table>
      </TableContainer>
    )
  }

  render() {
    return (
      this.props.movies.length ? this.getTable() : 
        this.props.searched ? <h1 className="CallForAction">No matches :(</h1> : <h1 className="CallForAction">Search for a movie!</h1>
    )
  }
}

export default MovieTable;