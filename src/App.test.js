import { render, screen, fireEvent } from '@testing-library/react';
import App from './App';

test('renders empty text', () => {
  render(<App />);
  const linkElement = screen.getByText(/Search for a movie!/i);
  expect(linkElement).toBeInTheDocument();
});

test('has input', () => {
  render(<App />);
  const input = screen.getByPlaceholderText('Search movies')
  expect(input).toBeInTheDocument();
});

test('does search without details', async () => {
  render(<App />);
  const input = screen.getByPlaceholderText('Search movies')
  const button = screen.getByText('Search')
  fireEvent.keyUp(input, {target: {value: 'Moonrise Kingdom'}})
  fireEvent.click(button)
  await new Promise((resolve) => setTimeout(resolve, 2000));
  const genres = screen.getByText(/Comedy, Drama, Romance/i)
  expect(genres).toBeInTheDocument()
  const link = screen.getByText(/Moonrise Kingdom: Welcome to the Island of New Penzance/i)
  fireEvent.click(link)
  await new Promise((resolve) => setTimeout(resolve, 2000));
  const error = screen.getByText(/Sorry, could not fetch more information./i)
  expect(error).toBeInTheDocument();
});

test('does search with details', async () => {
  render(<App />);
  const input = screen.getByPlaceholderText('Search movies')
  fireEvent.keyUp(input, {target: {value: 'Batman & Robin'}})
  const button_ = screen.getByText('Search')
  fireEvent.click(button_)
  await new Promise((resolve) => setTimeout(resolve, 2000));
  const link = screen.getByText('Batman vs. Robin')
  expect(link).toBeInTheDocument();
  fireEvent.click(link)
  await new Promise((resolve) => setTimeout(resolve, 2000));
  const button = screen.getByText(/Show similar/i)
  expect(button).toBeInTheDocument();
});