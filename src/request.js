async function request(method, endpoint, payload) {
  return new Promise(async function (resolve) {
    payload = payload || '';
    let queryString = '';
    let headers = {};

    switch (payload.constructor.name) {
      case 'String':
        headers['Content-Type'] = 'application/json';
        break;
      case 'FormData':
        break;
      case 'Object':
      case 'Array':
        headers['Content-Type'] = 'application/json';
        payload = JSON.stringify(payload);
        break;
      default:
        let error = { error: true }
        throw error;
    }
    let options;
    switch (method) {
      case 'GET':
        if (payload.constructor.name === 'String' && payload.length) {
          queryString = '?' + payload;
        }
        options = {
          method: method,
          headers: headers
        };
        break;
      default:
        options = {
          method: method,
          headers: headers,
          body: payload
        };
        break;
    }
    // console.log(method, endpoint, queryString)
    let result = await fetch(encodeURI(endpoint + queryString), options);
    if (result.status === 200 || result.status === 204) {
      if (method === 'DELETE') {
        resolve(result);
      } else {
        let json = await result.json();
        // console.log('JSON', json)
        resolve(json);
      }
    } else {
      // console.log(result);
      resolve(null);
    }
  });
}

export default request;