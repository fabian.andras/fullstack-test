import React from 'react';
import MovieTable from './MovieTable';
import { Grid, AppBar, Toolbar, Button, IconButton, Input, CircularProgress } from '@material-ui/core';
import { ReactComponent as AppLogo } from './logo.svg'
import request from './request';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      searched: false,
      searchValue: '',
      movies: []
    }
  }

  onSearch = async (searchValue) => {
    if (searchValue === '') {
      this.setState({
        searched: false,
        movies: []
      });
      return;
    }
    this.setState({
      loading: true
    });
    let query = [
      'query SearchMovies {',
      '  searchMovies(query: "' + searchValue + '") {',
      '    id',
      '    name',
      '    score',
      '    genres {',
      '      name',
      '    }',
      '  }',
      '}'
    ].join('\n');
    let payload = {
      operationName: 'SearchMovies',
      query: query,
      variables: {}
    }
    let result = await request('POST', 'https://tmdb.sandbox.zoosh.ie/dev/graphql', payload);
    this.setState({
      movies: result.data.searchMovies,
      loading: false,
      searched: true
    })
    // console.log(result)
  }

  onKeyUp = (e) => {
    if (e.key === 'Enter') {
      this.onSearch(this.state.searchValue)
    } else {
      this.setState({ searchValue: e.target.value });
    }
  }

  render() {
    return (
      <div>
        <AppBar id="AppBar" position="static">
          <Toolbar>
            <Grid container spacing={3}>
              <Grid item xs={12} sm={1}>
                <IconButton edge="start" color="inherit" aria-label="menu" href="/">
                  <AppLogo id="AppLogo" />
                </IconButton>
              </Grid>
              <Grid item xs={12} sm={10}>
                <Input onKeyUp={this.onKeyUp} id="SearchBox" fullWidth={true} placeholder="Search movies" />
              </Grid>
              <Grid item xs={12} sm={1}>
                <Button onClick={() => this.onSearch(this.state.searchValue)} id="SearchButton" color="inherit">Search</Button>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
        {this.state.loading ? <CircularProgress id="CircularProgress" /> : <MovieTable movies={this.state.movies} searched={this.state.searched} />}
      </div>
    );
  }
}

export default App;
