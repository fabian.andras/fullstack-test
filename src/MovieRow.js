import React from 'react';
import { TableRow, TableCell, Link, Button, CircularProgress } from '@material-ui/core'
import request from './request';

class MovieRow extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      wikiLoaded: false,
      wikiContent: null
    }
  }

  loadWikipedia = async () => {
    if (this.state.wikiLoaded) {
      this.setState({ wikiLoaded: false });
    } else {
      if (this.state.wikiContent) {
        this.setState({ wikiLoaded: true });
      } else {
        this.setState({
          loading: true
        })
        let result = await request('GET', 'https://en.wikipedia.org/w/api.php', 'format=json&action=query&prop=extracts&exintro=1&explaintext=1&origin=*&redirects&titles=' + this.props.title.replace('&', 'and'));
        if (result && result.query && result.query.pages) {
          for (let key in result.query.pages) {
            result = result.query.pages[key];
            break;
          }
          if (result.extract) {
            this.setState({
              wikiContent: result.extract
            });
            // console.log('OK')
          }
        }
      }
      this.setState({
        loading: false,
        wikiLoaded: true
      })
    }
  }

  getWikiContent = () => {
    if (this.state.loading) {
      return (
        <TableRow>
          <TableCell colSpan={3}>
            <CircularProgress />
          </TableCell>
        </TableRow>
      )
    }

    if (this.state.wikiContent) {
      return (
        <TableRow>
          <TableCell colSpan={3}>
            {this.state.wikiContent}<br /><br />
            <Button className="LeftPad" target="_blank" href={'https://www.themoviedb.org/movie/' + this.props.url} color="primary" variant="outlined">Open on TMDB</Button>
            <Button className="LeftPad" target="_blank" href={'https://en.wikipedia.org/wiki/' + this.props.title} color="primary" variant="outlined">Open on Wikipedia</Button>
            {this.props.showingSimilar ? null : <Button className="LeftPad" href="#" onClick={() => this.showSimilar(this.props.idx)} color="secondary" variant="outlined">Show similar</Button>}
          </TableCell>
        </TableRow>
      )
    }

    return (
      <TableRow>
        <TableCell colSpan={3}>
          Sorry, could not fetch more information.<br /><br />
          <Button color="secondary" variant="outlined" onClick={this.loadWikipedia}>Close</Button>
        </TableCell>
      </TableRow>
    )
  }

  showSimilar = (idx) => {
    this.props.showSimilar(this.props.idx)
  }
  
  render() {
    return (
      <React.Fragment>
        <TableRow>
          <TableCell>
            <Link href="#" onClick={(e) => { e.preventDefault(); this.loadWikipedia() }}>
              {this.props.title}
            </Link>
          </TableCell>
          <TableCell>{this.props.genres}</TableCell>
          <TableCell>{this.props.score}</TableCell>
        </TableRow>
        {(this.state.loading || this.state.wikiLoaded) ? this.getWikiContent() : null}
      </React.Fragment>
    )
  }
}

export default MovieRow;